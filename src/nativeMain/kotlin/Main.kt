import com.soywiz.korim.bitmap.Bitmap32
import com.soywiz.korim.format.PNG
import com.soywiz.korim.format.readBitmap
import com.soywiz.korim.format.writeTo
import com.soywiz.korio.file.std.*
import kotlinx.coroutines.runBlocking

val SQUARE_WIDTH = 1417
val SQUARE_HEIGHT = 1417
val LINE_THIKNESS = 3
val BLACK = com.soywiz.korim.color.Colors.BLACK
val IMAGE_WIDTH = 7087
val IMAGE_HEIGHT = 4724

fun main(args: Array<String>): Unit {
    runBlocking {

        if (args.size > 1) {
            println("expecting 1 parameter: base folder but found ${args.size} instead")
            return@runBlocking
        }

        val pathArg = if (args.isNotEmpty()) args[0]
            .replace("~", userHomeVfs.absolutePath)
        else nativeCwd

        val path = (
                if (pathArg.startsWith("/")) pathArg
                else "${nativeCwd}/${pathArg}"
                )
            .replace("//", "/")
            .replace("/./", "/")

        println("using path $path")

        val files = localVfs(path).listNames()
            .sortedBy { it }
            .filter { it.startsWith("oid") }
            .filter { it.endsWith(".png") }

        if (files.isEmpty()) {
            println("could ot find any file named oid*.png")
            return@runBlocking
        }

        files.joinToString(
            separator = "\n",
            transform = { "\t$it" },
            prefix = "oid pictures\n",
            postfix = "\n"
        ).let { println(it) }

        val images = mutableListOf<Bitmap32>()
        lateinit var img: Bitmap32

        files.withIndex().forEach { (index, file) ->
            val squareIndex = index % 12
            if (squareIndex == 0) {

                img = Bitmap32(IMAGE_WIDTH, IMAGE_HEIGHT, false)
                images.add(img)
            }
            val square = localVfs("$path/$file").readBitmap().toBMP32()
            val squareWidth = square.width
            val squareHeight = square.height

            if (squareWidth != SQUARE_WIDTH || squareHeight != SQUARE_HEIGHT) {
                throw Exception("found squareWidth $squareWidth and squareHeight $squareHeight")
            }

            val squareRow = squareIndex / 4
            val squareColumn = squareIndex % 4

            val startY = squareRow * (squareWidth + LINE_THIKNESS)
            val startX = squareColumn * (squareHeight + LINE_THIKNESS)
            val endX = startX + squareWidth
            val endY = startY + squareHeight

            for (x in 0 until squareWidth) {
                for (y in 0 until squareHeight) {
                    val pixel = square.getRgba(x, y)
                    img.setRgba(startX + x, startY + y, pixel)
                }
            }

            for (x in 0 until squareWidth) {
                for (t in 0 until LINE_THIKNESS) {
                    img.setRgba(startX + x, endY + t, BLACK)
                }
            }

            for (y in 0 until squareHeight) {
                for (t in 0 until LINE_THIKNESS) {
                    img.setRgba(endX + t, startY + y, BLACK)
                }
            }
        }

        images.withIndex().forEach { (index, image) ->
            val outPath = "${path}/out-${index+1}.png"
            println("writing file $outPath")
            image.writeTo(file = localVfs(outPath), formats = PNG)
        }

        if (images.isNotEmpty()) {
            println("please print the pictures with a resoultion of 1200 dpi, so they hava a size of 150 x 100 mm")
        }
    }
}